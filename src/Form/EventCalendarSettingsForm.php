<?php

/**
 * @file
 * Contains \Drupal\event_calendar\Form\EventCalendarSettingsForm
 */
namespace Drupal\event_calendar\Form;

use Drupal;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Cache\Cache;

/**
 * Configure event_calendar settings for this site.
 */
class EventCalendarSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'event_calendar_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array
  {
    return [
      'event_calendar.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array
  {

    $config = $this->config('event_calendar.module_settings');


    // General Settings
    $form['event_calendar_general_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('General Settings'),
      '#open' => TRUE,
    ];

    $form['event_calendar_general_settings']['events_calendar_base_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Events Listing Base Path'),
      '#description' => $this->t("Set the base path for links within the calendar filter (e.g., \"/events\"). Leave this blank if you always want to use the current page."),
      '#default_value' => $config->get('calendar_base_path'),
    ];


    // Data Push
    $form['event_calendar_data_push'] = [
      '#type' => 'details',
      '#title' => $this->t('Event Calendar Data Push'),
      '#open' => TRUE,
      '#description' => $this->t("<b>Note:</b> The following fields should be used only if this is the <b>source</b> site."),
    ];

    $form['event_calendar_data_push']['event_calendar_external_cron_link'] = [
      '#type' => 'textfield',
      '#title' => $this->t('External Cron Link'),
      '#description' => $this->t("On the destination site, go to <b>Configuration > System > Cron</b> to find the link to \"run cron from outside the site\"."),
      '#default_value' => $config->get('external_cron_link'),
    ];


    $form['event_calendar_data_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Event Calendar Data Pull'),
      '#open' => TRUE,
      '#description' => $this->t("<b>Note:</b> The following fields should be used only if this is the <b>destination</b> site."),
    ];

    $form['event_calendar_data_settings']['event_calendar_json_base_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Base Endpoint URL'),
      '#description' => $this->t('The base URL of the JSON:API enabled site which will serve as the source for importing events.'),
      '#default_value' => $config->get('json_base_url'),
    ];

    // Content Types
    $form['event_calendar_data_settings']['event_calendar_content_types'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Content Types'),
      '#open' => TRUE,
    ];

    $form['event_calendar_data_settings']['event_calendar_content_types']['event_calendar_json_events'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Events'),
      '#description' => $this->t('The path of the JSON:API feed which will serve as the source for importing events.'),
      '#default_value' => $config->get('json_events'),
    ];

    $form['event_calendar_data_settings']['event_calendar_content_types']['event_calendar_json_monthly_features'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Monthly Features'),
      '#description' => $this->t('The path of the JSON:API feed which will serve as the source for importing monthly features.'),
      '#default_value' => $config->get('json_monthly_features'),
    ];

    // Taxonomies
    $form['event_calendar_data_settings']['event_calendar_taxonomies'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Taxonomies'),
      '#open' => TRUE,
    ];

    $form['event_calendar_data_settings']['event_calendar_taxonomies']['event_calendar_json_event_categories'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Event Categories'),
      '#description' => $this->t('The path of the JSON:API feed which will serve as the source for importing event categories.'),
      '#default_value' => $config->get('json_event_categories'),
    ];

    $form['event_calendar_data_settings']['event_calendar_taxonomies']['event_calendar_json_event_locations'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Event Locations'),
      '#description' => $this->t('The path of the JSON:API feed which will serve as the source for importing event locations.'),
      '#default_value' => $config->get('json_event_locations'),
    ];


    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = Drupal::service('config.factory')->getEditable('event_calendar.module_settings');
    $config
      ->set('json_base_url', $form_state->getValue('event_calendar_json_base_url'))
      ->set('json_events', trim($form_state->getValue('event_calendar_json_events'), '/'))
      ->set('json_event_categories', trim($form_state->getValue('event_calendar_json_event_categories'), '/'))
      ->set('json_event_locations', trim($form_state->getValue('event_calendar_json_event_locations'), '/'))
      ->set('json_monthly_features', trim($form_state->getValue('event_calendar_json_monthly_features'), '/'))
      ->set('external_cron_link', $form_state->getValue('event_calendar_external_cron_link'))
      ->set('calendar_base_path', $form_state->getValue('events_calendar_base_path'))
      ->save();

    Cache::invalidateTags(array('config:event_calendar.module_settings'));

  }
}
