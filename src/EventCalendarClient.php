<?php

namespace Drupal\event_calendar;

use Drupal;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Http\ClientFactory;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class EventCalendarClient
{

  /**
   * @var Client
   */
  protected Client $client;

  /**
   * @var string
   */
  protected string $credentials;

  /**
   * @param $http_client_factory ClientFactory
   */
  public function __construct(ClientFactory $http_client_factory)
  {
    $config = Drupal::service('config.factory')->get('event_calendar.module_settings');
    $this->credentials = $config->get('credentials');

    $this->client = $http_client_factory->fromOptions([
      'base_uri' => $config->get('json_base_url'),
    ]);
  }

  /**
   * @param string $start_date
   * @param string $end_date
   * @return mixed
   * @throws GuzzleException
   */
  public function getEvents(string $start_date = '', string $end_date = ''): mixed
  {
    $options = [];

    if ($this->credentials) {
      $options['headers'] = [
        'Authorization' => 'Basic ' . $this->credentials,
      ];
    }

    if ($start_date != '' && $end_date != '') {

      $options['query'] = [
        'views-filter[start]' => $start_date,
        'views-filter[end]' => $end_date,
      ];
      $response = $this->client->get('jsonapi/views/events/block_events_listing', $options);

    } else {
      $response = $this->client->get('jsonapi/node/event', $options);
    }

    $json = Json::decode($response->getBody());

    return $json['data'];
  }

  /**
   * @param $id
   * @return mixed
   * @throws GuzzleException
   */
  public function getEvent($id): mixed
  {
    $options = [];

    if ($this->credentials) {
      $options['headers'] = [
        'Authorization' => 'Basic ' . $this->credentials,
      ];
    }

    $options['query'] = [
      'id' => $id,
    ];

    $response = $this->client->get('jsonapi/node/event', $options);
    $json = Json::decode($response->getBody());

    return $json['data'];
  }
}
