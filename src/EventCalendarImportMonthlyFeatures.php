<?php

namespace Drupal\event_calendar;

use Drupal;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Http\ClientFactory;
use Drupal\node\Entity\Node;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class EventCalendarImportMonthlyFeatures.
 */
class EventCalendarImportMonthlyFeatures
{

  /**
   * @var Client
   */
  protected Client $client;

  /**
   * @var string
   */
  protected string $credentials;

  /**
   * @var array
   */
  protected array $json_decoded = [];

  /**
   * @param $http_client_factory ClientFactory
   */
  public function __construct(ClientFactory $http_client_factory)
  {
    $config = Drupal::service('config.factory')->get('event_calendar.module_settings');
    $this->credentials = $config->get('credentials') ?: '';

    $this->client = $http_client_factory->fromOptions([
      'base_uri' => $config->get('json_base_url'),
    ]);
  }

  /**
   * @return void
   * @throws GuzzleException|EntityStorageException
   */
  public function runImport()
  {

    $config = Drupal::config('event_calendar.module_settings');

    $options = [];
    if ($this->credentials) {
      $options['headers'] = [
        'Authorization' => 'Basic ' . $this->credentials,
      ];
    }

    // Import Monthly Features.
    $json_path = $config->get('json_monthly_features') . '?' . time();
    $response = $this->client->get($json_path, $options);
    $this->json_decoded = Json::decode($response->getBody());
    $json_data = $this->json_decoded['data'];

    echo 'Attempting to import monthly features from ' . $config->get('json_base_url') . $json_path . PHP_EOL;

    $num_records = count($json_data);

    if ($num_records == 0) {
      echo 'No monthly features to import.' . PHP_EOL;
    } else {
      echo 'Processing ' . $num_records . ' Node' . ($num_records > 1 ? 's' : '') . ':' . PHP_EOL;

      // Make sure data is in correct format before processing anything.
      $processed_ids = [];
      foreach ($json_data as $data) {
        $nid = $data['attributes']['drupal_internal__nid'];
        $isAlreadyImported = $this->isAlreadyImported($nid);
        if (!$isAlreadyImported) {
          $processed_ids[] = $this->import($data);
        } else {
          $processed_ids[] = $this->update($data, $isAlreadyImported);
        }
      }

      $this->prune($processed_ids);
      if (!empty($this->warnings)) {
        echo PHP_EOL . 'WARNINGS' . PHP_EOL;
        foreach ($this->warnings as $warning) {
          echo $warning . PHP_EOL;
        }
      }
    }
  }

  /**
   * @param $external_nid
   * @return mixed
   */
  public function isAlreadyImported($external_nid): mixed
  {
    try {
      $result = Drupal::entityQuery('node')
        ->condition('type', 'monthly_feature')
        ->condition('field_external_node_id', $external_nid, '=')
        ->accessCheck(FALSE)
        ->execute();
      if (!empty($result)) {
        foreach ($result as $record) {
          return $record;
        }
      }
    } catch (Exception $e) {
      echo $e->getMessage() . PHP_EOL;
    }
    return false;
  }

  /**
   * Import the event category data.
   * @param $data
   * @return int|mixed|string|null
   * @throws EntityStorageException
   */
  private function import($data)
  {
    // Import Event
    $node = Node::create(['type' => 'monthly_feature']);
    $this->setValues($node, $data);

    echo '- Node inserted: nid (' . $node->id() . ')' . PHP_EOL;

    return $node->id();

  }

  /**
   * Update the imported nodes (for entity references that did not exist on import).
   * @param $data
   * @param $nid
   * @return int|mixed|string|void|null
   * @throws EntityStorageException
   */
  private function update($data, $nid)
  {
    if (empty($nid)) {
      return;
    }

    $node = Node::load($nid);
    if ($node instanceof Node === FALSE) {
      return;
    }

    $this->setValues($node, $data);

    echo '- Node updated: nid (' . $node->id() . ')' . PHP_EOL;

    return $node->id();

  }

  /**
   * @return mixed|void
   * @throws Drupal\Core\Entity\Sql\SqlContentEntityStorageException
   */
  private function prune($processed_ids)
  {

    try {
      $result = Drupal::entityQuery('node')
        ->condition('type', 'monthly_feature')
        ->condition('field_external_node_id', '', '<>')
        ->accessCheck(FALSE)
        ->execute();

      if (is_array($result)) {
        $prune_ids = array_diff($result, $processed_ids);
        $num_records = count($prune_ids);
        if ($num_records >= 1) {
          echo 'Pruning ' . $num_records . ' node' . ($num_records > 1 ? 's' : '') . ':' . PHP_EOL;
          foreach ($prune_ids as $nid) {
            echo '- Node pruned: nid (' . $nid . ')' . PHP_EOL;
            $node = Node::load($nid);
            $node->delete();
          }
        }
      }
    } catch (Exception $e) {
      echo $e->getMessage() . PHP_EOL;
    }
  }

  /**
   * @throws EntityStorageException
   */
  private function setValues(Node $event, array $data)
  {
    $event->setPublished();

    // Event Title & Source NID:
    $event->set('title', htmlspecialchars_decode(($data['attributes']['title'] ?: '')));
    $event->set('field_external_node_id', $data['attributes']['drupal_internal__nid']);

    // Date Range
    $smart_date = [
      'rrule' => $data['attributes']['field_event_date']['rrule'],
      'rrule_index' => $data['attributes']['field_event_date']['rrule_index'],
      'value' => strtotime($data['attributes']['field_event_date']['value']),
      'end_value' => strtotime($data['attributes']['field_event_date']['end_value']),
      'duration' => $data['attributes']['field_event_date']['duration'],
      'timezone' => $data['attributes']['field_event_date']['timezone'] ?: 'UTC',
    ];
    $event->set('field_event_date', $smart_date);

    // Message field:
    $message = htmlspecialchars_decode(($data['attributes']['field_message']['value'] ?: ''));
    $message = Drupal::service('event_calendar.import_helper')->processHtmlImages($message, 'public://event-images/');
    $event->set('field_message', [
      'value' => $message,
      'format' => $data['attributes']['field_message']['format'],
    ]);

    // Other fields:
    $event->set('field_link', $data['attributes']['field_link']);

    $event->set("path", ["pathauto" => TRUE]);
    $event->save();

  }
}
