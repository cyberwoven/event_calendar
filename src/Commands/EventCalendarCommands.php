<?php

/**
 * Annotations available for use include:
 * @command – command definition, which needs to follow the module: command structure;
 * @aliases – aliases for your commands, separated with spaces;
 * @param – which defines the input parameters for your command;
 * @option – which defines the options available for your commands, which should be put in an associative array, where the name of the option is the key;
 * @usage – example showing how the command should be used.
 */

namespace Drupal\event_calendar\Commands;

use Drupal;
use Drush\Commands\DrushCommands;


/**
 * Drush command file.
 */
class EventCalendarCommands extends DrushCommands
{

  /**
   * A custom Drush command to displays the given text.
   *
   * @command import:print-json
   */
  public function print_aging_events_json()
  {
    $importService = Drupal::service('event_calendar.event_calendar_import');
    $importService->printJsonDecoded();
  }

  /**
   * A custom Drush command to import/update events.
   * @command import:aging-events
   * @option print
   *   - print - Print the decoded json.
   */
  public function import_aging_events($options = ['print'])
  {

    if ($options['print']) {

      $importService = Drupal::service('event_calendar.event_calendar_import');
      $importService->printJsonDecoded();

    } else {

      $importEventCategories = Drupal::service('event_calendar.import_event_categories');
      $this->output()->writeln('Beginning aging.sc.gov events categories import...');
      $importEventCategories->runImport();

      $importEventLocations = Drupal::service('event_calendar.import_event_locations');
      $this->output()->writeln('Beginning aging.sc.gov events locations import...');
      $importEventLocations->runImport();

      $importEvents = Drupal::service('event_calendar.import_events');
      $this->output()->writeln('Beginning aging.sc.gov events import...');
      $importEvents->runImport();

      $importMonthlyFeatures = Drupal::service('event_calendar.import_monthly_features');
      $this->output()->writeln('Beginning aging.sc.gov monthly features import...');
      $importMonthlyFeatures->runImport();

      $this->output()->writeln('Finished aging.sc.gov events import.');

    }
  }
}
