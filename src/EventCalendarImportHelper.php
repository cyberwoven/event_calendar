<?php

namespace Drupal\event_calendar;

use Drupal;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Http\ClientFactory;
use Drupal\filter\FilterProcessResult;
use Drupal\node\Entity\Node;
use http\Client;

/**
 * Class EventCalendarImportHelper.
 */
class EventCalendarImportHelper
{

  /**
   * This fetches the remote image, saves it to the filesystem and sets the node field.
   *
   * @param Node $node
   *   The node that will be modified.
   *
   * @param string $image_url
   *   The source image's URL.
   *
   * @param string $dest_field_name
   *   The d8 field name.
   *
   * @param string $dest_dir
   *   The directory in which to save the image.
   *
   * @param array $warnings
   *   An array to pass warnings to. (Yes this is a hack but its convenient for now.)
   */
  static function setImageFieldFromRemote(Node &$node, string $image_url, string $dest_field_name, string $dest_dir, array &$warnings)
  {

    if (!empty($image_url)) {
      // Create file object from remote URL.
      $exploded_image_url = explode("/", $image_url);
      $image_url_last = end($exploded_image_url);
      $exploded_image_url_last = explode("?", $image_url_last);
      $image_filename = $exploded_image_url_last[0];
      $image_blob = self::getRemoteFileContents($image_url);
      $image_dest_path = $dest_dir . $image_filename;

      if (Drupal::service('file_system')->prepareDirectory($dest_dir, FileSystemInterface::CREATE_DIRECTORY)) {
        $file = Drupal::service('file.repository')->writeData($image_blob, $image_dest_path, FileSystemInterface::EXISTS_REPLACE);

        if (!empty($file)) {
          $node->set($dest_field_name, [
            'target_id' => $file->id(),
            'alt' => '',
            'title' => '',
          ]);
        } else {
          $warnings[] = 'Failed to save image. File could not be saved.';
        }
      } else {
        $warnings[] = 'Failed to save image. Directory could not be created.';
      }
    }
  }

  /**
   * Fetch the remote images in an HTML string, save them to the filesystem, and set img src attributes to the URLs of the saved images.
   *
   * @param string $text
   *   The string of HTML that will be modified.
   *
   * @param string $origin
   *   The origin site where the images should be pulled. Image URLs will most likely be relative.
   *
   * @param string $dest_dir
   *   The directory in which to save the image.
   */
  static function processHtmlImages(string $text, string $dest_dir): FilterProcessResult
  {
    $config = Drupal::config('event_calendar.module_settings');
    $dom = Html::load($text);
    foreach ($dom->getElementsByTagName('img') as $element) {
      $src = $element->getAttribute('src');
      if (!$src) {
        continue;
      }

      // If src is a relative URL
      if (!str_contains($src, '//')) {
        // Remove trailing slash from origin.
        $origin = rtrim($config->get('json_base_url'), '/');
        $src = $origin . $src;
      }
      // Create file object from remote URL.
      $exploded_image_url = explode("/", $src);
      $image_url_last = end($exploded_image_url);
      $exploded_image_url_last = explode("?", $image_url_last);
      $image_filename = $exploded_image_url_last[0];
      $image_blob = self::getRemoteFileContents($src);
      $image_dest_path = $dest_dir . $image_filename;
      if (Drupal::service('file_system')->prepareDirectory($dest_dir, FileSystemInterface::CREATE_DIRECTORY)) {
        $file_repository = Drupal::service('file.repository');
        $file_repository->writeData($image_blob, $image_dest_path, FileSystemInterface::EXISTS_REPLACE);
      }
      $absolute_url = Drupal::service('file_url_generator')->generateAbsoluteString(($image_dest_path));
      $relative_image_path = Drupal::service('file_url_generator')->transformRelative($absolute_url);
      $element->setAttribute('src', $relative_image_path);
    }
    return new FilterProcessResult(Html::serialize($dom));
  }

  /**
   * @param $filename
   * @return false|string
   */
  static function getRemoteFileContents($filename): bool|string
  {

    $config = Drupal::config('event_calendar.module_settings');

    $context = stream_context_create(array(
      "http" => array(
        "header" => "Authorization: Basic " . $config->get('credentials'),
        "protocol_version" => 1.1,
      )));

    return file_get_contents($filename, false, $context);
  }

}
