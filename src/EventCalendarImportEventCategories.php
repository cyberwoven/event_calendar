<?php

namespace Drupal\event_calendar;

use Drupal;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Http\ClientFactory;
use Drupal\taxonomy\Entity\Term;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class EventCalendarImportEventCategories.
 */
class EventCalendarImportEventCategories
{

  /**
   * @var Client
   */
  protected Client $client;

  /**
   * @var string
   */
  protected string $credentials;

  /**
   * @var array
   */
  protected array $json_decoded = [];

  /**
   * @param $http_client_factory ClientFactory
   */
  public function __construct(ClientFactory $http_client_factory)
  {
    $config = Drupal::service('config.factory')->get('event_calendar.module_settings');
    $this->credentials = $config->get('credentials') ?: '';

    $this->client = $http_client_factory->fromOptions([
      'base_uri' => $config->get('json_base_url'),
    ]);
  }

  /**
   * @return void
   * @throws GuzzleException|EntityStorageException
   */
  public function runImport()
  {

    $config = Drupal::config('event_calendar.module_settings');

    $options = [];
    if ($this->credentials) {
      $options['headers'] = [
        'Authorization' => 'Basic ' . $this->credentials,
      ];
    }

    // Import Event Categories.
    $json_path = $config->get('json_event_categories') . '?' . time();
    $response = $this->client->get($json_path, $options);
    $this->json_decoded = Json::decode($response->getBody());
    $json_data = $this->json_decoded['data'];

    echo 'Attempting to import event categories from ' . $config->get('json_base_url') . $json_path . PHP_EOL;

    $num_records = count($json_data);

    if ($num_records == 0) {
      echo 'No event categories to import.' . PHP_EOL;
    } else {
      echo 'Processing ' . $num_records . ' Term' . ($num_records > 1 ? 's' : '') . ':' . PHP_EOL;

      // Make sure data is in correct format before processing anything.
      foreach ($json_data as $data) {
        $isAlreadyImported = $this->isAlreadyImported($data['attributes']['drupal_internal__tid']);

        if (!$isAlreadyImported) {
          $this->import($data);
        } else {
          $this->update($data, $isAlreadyImported);
        }
      }
    }
  }

  /**
   * @param array $external_term_ids
   * @return array|int
   */
  public function getInternalTermIds(array $external_term_ids): array|int
  {

    $internal_term_ids = [];
    if (!empty($external_term_ids)) {
      $query = Drupal::entityQuery('taxonomy_term')
        ->condition('vid', 'categories')
        ->condition('field_external_term_id', $external_term_ids, 'in')
        ->accessCheck(FALSE);
      $internal_term_ids = $query->execute();
    }

    return $internal_term_ids;

  }

  /**
   * @param $external_tid
   * @return mixed
   */
  public function isAlreadyImported($external_tid): mixed
  {
    try {
      $result = Drupal::entityQuery('taxonomy_term')
        ->condition('vid', 'categories')
        ->condition('field_external_term_id', $external_tid, '=')
        ->accessCheck(FALSE)
        ->execute();
      if (!empty($result)) {
        foreach ($result as $record) {
          return $record;
        }
      }
    } catch (Exception $e) {
      echo $e->getMessage() . PHP_EOL;
    }
    return false;
  }

  /**
   * Import the event category data.
   * @throws EntityStorageException
   */
  private function import($data)
  {
    // Import Event Categories
    $term = Term::create(['vid' => 'categories']);
    $this->setValues($term, $data);

    echo '- Term inserted: tid (' . $term->id() . ')' . PHP_EOL;
  }

  /**
   * Update the imported nodes (for entity references that did not exist on import).
   * @throws EntityStorageException
   */
  private function update($data, $tid)
  {
    if (empty($tid)) {
      return;
    }

    $term = Term::load($tid);
    if ($term instanceof Term === FALSE) {
      return;
    }

    $this->setValues($term, $data);

    echo '- Term updated: tid (' . $term->id() . ')' . PHP_EOL;

  }

  /**
   * @param Term $category
   * @param array $data
   * @return void
   */
  private function setValues(Term $category, array $data)
  {

    try {

      $category->setPublished();

      // Event Title & Source TID:
      $category->set('name', (htmlspecialchars_decode($data['attributes']['name'] ?: '')));
      $category->set('description', $data['attributes']['description']);
      $category->set('field_external_term_id', $data['attributes']['drupal_internal__tid']);

      $category->set("path", ["pathauto" => TRUE]);
      $category->save();

    } catch (Exception $e) {
      echo $e->getMessage() . PHP_EOL;
    }

  }
}
