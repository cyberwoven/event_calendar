<?php

namespace Drupal\event_calendar;

use Drupal;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Http\ClientFactory;
use Drupal\node\Entity\Node;
use Drupal\system\TimeZoneResolver;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class EventCalendarImportEvents.
 */
class EventCalendarImportEvents
{

  /**
   * @var Client
   */
  protected Client $client;

  /**
   * @var string
   */
  protected string $credentials;

  /**
   * @var array
   */
  protected array $json_decoded = [];

  /**
   * @var array
   */
  protected array $warnings = [];

  /**
   * @param $http_client_factory ClientFactory
   */
  public function __construct(ClientFactory $http_client_factory)
  {
    $config = Drupal::service('config.factory')->get('event_calendar.module_settings');
    $this->credentials = $config->get('credentials') ?: '';

    $this->client = $http_client_factory->fromOptions([
      'base_uri' => $config->get('json_base_url'),
    ]);
  }

  /**
   * @return void
   * @throws GuzzleException|EntityStorageException
   */
  public function runImport()
  {

    $config = Drupal::config('event_calendar.module_settings');

    $options = [];
    if ($this->credentials) {
      $options['headers'] = [
        'Authorization' => 'Basic ' . $this->credentials,
      ];
    }

    // Import Events.
    $json_path = $config->get('json_events') . '?' . time();
    $response = $this->client->get($json_path, $options);
    $this->json_decoded = Json::decode($response->getBody());
    $json_data = $this->json_decoded['data'];

    echo 'Attempting to import events from ' . $config->get('json_base_url') . $json_path . PHP_EOL;

    $num_records = count($json_data);

    if ($num_records == 0) {
      echo 'No events to import.' . PHP_EOL;
    } else {
      echo 'Processing ' . $num_records . ' node' . ($num_records > 1 ? 's' : '') . ':' . PHP_EOL;

      $processed_ids = [];

      // Make sure data is in correct format before processing anything.
      foreach ($json_data as $data) {
        $nid = $data['attributes']['drupal_internal__nid'];
        $isAlreadyImported = $this->isAlreadyImported($nid);
        if (!$isAlreadyImported) {
          $processed_ids[] = $this->import($data);
        } else {
          $processed_ids[] = $this->update($data, $isAlreadyImported);
        }
      }

      $this->prune($processed_ids);

      if (!empty($this->warnings)) {
        echo PHP_EOL . 'WARNINGS' . PHP_EOL;
        foreach ($this->warnings as $warning) {
          echo $warning . PHP_EOL;
        }
      }
    }
  }

  /**
   * @param $external_nid
   * @return mixed
   */
  public function isAlreadyImported($external_nid): mixed
  {
    try {
      $result = Drupal::entityQuery('node')
        ->condition('type', 'event')
        ->condition('field_external_node_id', $external_nid, '=')
        ->accessCheck(FALSE)
        ->execute();
      if (!empty($result)) {
        foreach ($result as $record) {
          return $record;
        }
      }
    } catch (Exception $e) {
      echo $e->getMessage() . PHP_EOL;
    }
    return false;
  }

  /**
   * Import the event category data.
   * @param $data
   * @return int|mixed|string|null
   * @throws EntityStorageException
   */
  private function import($data)
  {
    // Import Event
    $node = Node::create(['type' => 'event']);
    $this->setValues($node, $data);

    echo '- Node inserted: nid (' . $node->id() . ')' . PHP_EOL;

    return $node->id();
  }

  /**
   * Update the imported nodes (for entity references that did not exist on import).
   * @param $data
   * @param $nid
   * @return int|mixed|string|void|null
   * @throws EntityStorageException
   */
  private function update($data, $nid)
  {
    if (empty($nid)) {
      return;
    }

    $node = Node::load($nid);
    if ($node instanceof Node === FALSE) {
      return null;
    }

    $this->setValues($node, $data);

    echo '- Node updated: nid (' . $node->id() . ')' . PHP_EOL;

    return $node->id();

  }

  /**
   * @return mixed|void
   * @throws Drupal\Core\Entity\Sql\SqlContentEntityStorageException
   */
  private function prune($processed_ids)
  {

    try {
      $result = Drupal::entityQuery('node')
        ->condition('type', 'event')
        ->condition('field_external_node_id', '', '<>')
        ->accessCheck(FALSE)
        ->execute();

      if (is_array($result)) {
        $prune_ids = array_diff($result, $processed_ids);
        $num_records = count($prune_ids);
        if ($num_records >= 1) {
          echo 'Pruning ' . $num_records . ' node' . ($num_records > 1 ? 's' : '') . ':' . PHP_EOL;
          foreach ($prune_ids as $nid) {
            echo '- Node pruned: nid (' . $nid . ')' . PHP_EOL;
            $node = Node::load($nid);
            $node->delete();
          }
        }
      }
    } catch (Exception $e) {
      echo $e->getMessage() . PHP_EOL;
    }
  }

  /**
   * @throws EntityStorageException
   */
  private function setValues(Node $event, array $data)
  {

    // Event Title & Source NID:
    $event->set('title', htmlspecialchars_decode(($data['attributes']['title'] ?: '')));
    $event->set('field_external_node_id', $data['attributes']['drupal_internal__nid']);

    // Date Range
    $smart_date = [
      'rrule' => $data['attributes']['field_event_date']['rrule'],
      'rrule_index' => $data['attributes']['field_event_date']['rrule_index'],
      'value' => strtotime($data['attributes']['field_event_date']['value']),
      'end_value' => strtotime($data['attributes']['field_event_date']['end_value']),
      'duration' => $data['attributes']['field_event_date']['duration'],
      'timezone' => $data['attributes']['field_event_date']['timezone'] ?: 'UTC',
    ];
    $event->set('field_event_date', $smart_date);

    // Body:
    $body = htmlspecialchars_decode(($data['attributes']['body']['value'] ?: ''));
    $body = Drupal::service('event_calendar.import_helper')->processHtmlImages($body, 'public://event-images/');
    $event->set('body', [
      'value' => $body,
      'format' => 'full_html',
      'summary' => $data['attributes']['body']['summary'],
    ]);

    // Time, Address (Location) & Contact Information:
    $event->set('field_event_time', $data['attributes']['field_event_time']);
    $event->set('field_event_location', $data['attributes']['field_event_location']);
    $event->set('field_event_location_text', (htmlspecialchars_decode($data['attributes']['field_event_location_text'] ?: '')));
    $event->set('field_event_phone', $data['attributes']['field_event_phone']);
    $event->set('field_event_website', $data['attributes']['field_event_website']);
    $event->set('field_event_contact_name', $data['attributes']['field_event_contact_name']);
    $event->set('field_event_contact_email', $data['attributes']['field_event_contact_email']);
    $event->set('field_featured', $data['attributes']['field_feature_on_getcare']);
    $event->set('field_virtual_event', $data['attributes']['field_virtual_event']);

    // Event Categories:
    $external_term_ids = [];
    if (!empty($data['relationships']['field_event_categories']['data'])) {
      foreach ($data['relationships']['field_event_categories']['data'] as $event_category) {
        $external_term_ids[] = $event_category['meta']['drupal_internal__target_id'];
      }
      $target_ids = Drupal::service('event_calendar.import_event_categories')->getInternalTermIds($external_term_ids);
      $event->set('field_event_categories', $target_ids);
    }

    // Event Locations (Region):
    $external_term_ids = [];
    if (!empty($data['relationships']['field_event_region']['data'])) {
      foreach ($data['relationships']['field_event_region']['data'] as $event_region) {
        $external_term_ids[] = $event_region['meta']['drupal_internal__target_id'];
      }
      $target_ids = Drupal::service('event_calendar.import_event_locations')->getInternalTermIds($external_term_ids);
      $event->set('field_event_region', $target_ids);
    }
    
    // Featured Image:
    if (!empty($data['relationships']['field_image']['data'])) {
      $image_url = $this->getImageFieldUrl($data['relationships']['field_image']);
      Drupal::service('event_calendar.import_helper')->setImageFieldFromRemote(
        $event, $image_url, 'field_image', 'public://event-images/featured/', $this->warnings
      );
    }

    // Set published status.
    if ($data['attributes']['status']) {
      $event->setPublished();
    } else {
      $event->setUnpublished();
    }

    $event->set("path", ["pathauto" => TRUE]);
    $event->save();
  }

  private function getImageFieldUrl($image_data)
  {

    $config = Drupal::config('event_calendar.module_settings');

    $related_link = $image_data['links']['related']['href'];
    $base_url = $config->get('json_base_url');

    if (str_starts_with($related_link, $base_url)) {
      $related_link_path = substr($related_link, strlen($base_url));

      $options = [];
      if ($this->credentials) {
        $options['headers'] = [
          'Authorization' => 'Basic ' . $this->credentials,
        ];
      }

      $response = $this->client->get($related_link_path, $options);
      $json_decoded = Json::decode($response->getBody());
      $json_data = $json_decoded['data']['attributes']['uri']['url'];

      return $config->get('json_base_url') . ltrim($json_data, '/');

    }
  }
}
