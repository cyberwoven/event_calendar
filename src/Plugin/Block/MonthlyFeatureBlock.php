<?php

namespace Drupal\event_calendar\Plugin\Block;

use Drupal;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\node\Entity\Node;

/**
 * Provides a 'Event Calendar - Monthly Feature' block.
 *
 * @Block(
 *  id = "event_calendar__monthly_feature",
 *  admin_label = @Translation("Event Calendar - Monthly Feature"),
 * )
 */
class MonthlyFeatureBlock extends BlockBase implements BlockPluginInterface
{

  /**
   * {@inheritdoc}
   */
  public function build()
  {

//    $features = $this->getMonthlyFeature();

    // Build the block.
    return [
      '#theme' => 'event_calendar__monthly_feature',
      '#items' => $this->getMonthlyFeatures(),
      '#cache' => [
        'max-age' => 0,
      ],
    ];

  }

  private function getMonthlyFeatures(): mixed
  {

    $features = [];

    $current_time = time();

    // Get all monthly features for the current month.
    $query = Drupal::entityQuery('node')
      ->condition('type', 'monthly_feature')
      ->condition('status', '1')
      ->condition('field_event_date.value', $current_time, '<=')
      ->condition('field_event_date.end_value', $current_time, '>=')
      ->accessCheck(TRUE);

    $node_ids = $query->execute();
    $nodes = Node::loadMultiple($node_ids);

    foreach ($nodes as $node) {
      $features[] = Drupal::entityTypeManager()->getViewBuilder('node')->view($node, 'teaser');
    }

    return $features;


  }

}
