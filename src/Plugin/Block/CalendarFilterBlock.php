<?php

/**
 * @file
 * Contains \Drupal\event_calendar\Plugin\Block\CalendarFilterBlock.
 */

namespace Drupal\event_calendar\Plugin\Block;

use DateTimeZone;
use Drupal;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Provides a 'Event Calendar - Calendar Filter' block.
 *
 * @Block(
 *  id = "event_calendar__calendar_filter",
 *  admin_label = @Translation("Event Calendar - Calendar Filter"),
 * )
 */
class CalendarFilterBlock extends BlockBase
{

  /**
   * @return array
   */
  public function build(): array
  {

    $dates = $this->getDatesArray('Y-m-d');

    $build['event_calendar__calendar_filter'] = [
      '#theme' => 'event_calendar__calendar_filter',
      '#calendarContent' => [
        '#markup' => $this->createCalendar($dates),
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('event_calendar.event_calendar_client')
    );
  }

  /**
   * @param $format
   * @return array|string[]
   */
  private function getDatesArray($format): array
  {
    // Get Now and Future dates for conditions.
    $now = new DrupalDateTime('now');
    $now->setTimezone(new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE));
    $now = $now->getTimestamp();
    $future_date = strtotime('+24 months', $now);

    $events = [];

    // Make sure the event entity type and the event date field exist before querying.
    $field_definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions('node', 'event');
    if (in_array('field_event_date', array_keys($field_definitions))) {

      // Get all future events (up to duration).
      $query = \Drupal::entityQuery('node')
        ->condition('type', 'event')
        ->condition('status', '1')
        ->condition('field_event_date.end_value', $now, '>=')
        ->condition('field_event_date.end_value', $future_date, '<=')
        ->accessCheck(TRUE);

      $nids = $query->execute();
      $events = \Drupal\node\Entity\Node::loadMultiple($nids);
    }

    $dates = [];
    foreach ($events as $event) {

      $field_event_date = $event->get('field_event_date')->getValue();

      // Using the entity query.
      $src_timezone = date_default_timezone_get();
      $dest_timezone = $field_event_date[0]['timezone'];
      $start_date = $this->convertSmartDate($field_event_date[0]['value'], $src_timezone, $dest_timezone);
      $end_date = $this->convertSmartDate($field_event_date[0]['end_value'], $src_timezone, $dest_timezone) + $field_event_date[0]['duration'];

      $day_length = (60 * 60 * 24);
      $days = round(($end_date - $start_date) / $day_length);

      // Loop through days, and add to array
      for ($ii = 0; $ii < $days; $ii++) {
        $dates[] = $start_date + ($day_length * $ii);
      }
      $dates = array_unique($dates, SORT_NUMERIC);
    }

    $dates = array_map(function ($date) use ($format) {
      return date($format, $date);
    }, $dates);

    return $dates;
  }

  /**
   * @param $dates
   * @return mixed
   */
  private function createCalendar($dates): mixed
  {

    $config = Drupal::service('config.factory')->get('event_calendar.module_settings');
    $calendar_base_path = $config->get('calendar_base_path') ?: '';

    // Get Now and Future dates for conditions.
    $now = new DrupalDateTime('now');
    $now = $now->setTimezone(new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE))->getTimestamp();
    $current_month = strtotime(date('Y-m-01', $now)); // Calculate from the 1st ('cause every month has a 1st).

    for ($ii = 0; $ii < 24; $ii++) {
      $filter_month = strtotime('+' . $ii . ' month', $current_month);
      $filter_month_label = date('F Y', $filter_month);
      $filter_month_start_dow = date('w', $filter_month); // Month's starting day of week.
      $filter_month_num = date('m', $filter_month);
      $filter_year_num = date('Y', $filter_month);
      $filter_days_in_month = cal_days_in_month(CAL_GREGORIAN,
        $filter_month_num,
        $filter_year_num
      );

      $config = "";

      $filter_month_range_start = date('Y-m-d', strtotime('first day of ' . $filter_month_label));
      $filter_month_range_end = date('Y-m-d', strtotime('last day of ' . $filter_month_label));
      $filter_month_url = \Drupal\Core\Url::fromUserInput(
        $calendar_base_path . '?start=' . $filter_month_range_start . '&end=' . $filter_month_range_end
      );

      $table = [
        '#type' => 'html_tag',
        '#tag' => 'table',
      ];

      // Table Header
      $table['thead'] = [
        '#type' => 'html_tag',
        '#tag' => 'thead',
      ];

      $table['thead']['tr_month'] = [
        '#type' => 'html_tag',
        '#tag' => 'tr',
        'th_month' => [
          '#type' => 'html_tag',
          '#tag' => 'th',
          'month_link' => [
            '#title' => $filter_month_label,
            '#type' => 'link',
            '#url' => $filter_month_url,
            '#attributes' => [
              'aria-label' => 'See events in ' . $filter_month_label,
            ],
          ],
          '#attributes' => [
            'colspan' => 7,
            'class' => 'calendar-filter-month-label',
          ],
        ],
      ];

      $weekdays = ['S', 'M', 'T', 'W', 'T', 'F', 'S'];
      $table['thead']['tr_dow'] = [
        '#type' => 'html_tag',
        '#tag' => 'tr',
      ];
      foreach ($weekdays as $key => $weekday) {
        $table['thead']['tr_dow']['td_dow_' . $key] = [
          '#type' => 'html_tag',
          '#tag' => 'th',
          '#value' => $weekday,
        ];
      }

      // Table Body
      $table['tbody'] = [
        '#type' => 'html_tag',
        '#tag' => 'tbody',
      ];

      // Table Rows
      for ($row = 0; $row < 6; $row++) {
        $tr = [
          '#type' => 'html_tag',
          '#tag' => 'tr',
        ];
        for ($col = 1; $col <= 7; $col++) { // 1-based for easier maths.

          // Add dates to the calendar
          $cell_date_offset = $filter_month_start_dow;
          $cell_date_value = $col + ($row * 7) - $cell_date_offset;

          $td = [
            '#type' => 'html_tag',
            '#tag' => 'td',
          ];

          if ($cell_date_value > 0 && $cell_date_value <= $filter_days_in_month) {
            // Build date string.

            $cell_date_formatted = $filter_year_num . '-' .
              $filter_month_num . '-' .
              str_pad($cell_date_value, 2, '0', STR_PAD_LEFT);
            $cell_date = strtotime($cell_date_formatted);

            $cell_next_date = strtotime('+1 day', $cell_date);
            $cell_next_date_formatted = date('Y-m-d', $cell_next_date);

            $cell_date_url = \Drupal\Core\Url::fromUserInput(
              $calendar_base_path . '?start=' . $cell_date_formatted . '&end=' . $cell_date_formatted
            );

            if (in_array($cell_date_formatted, $dates)) {
              $td['content'] = [
                'date_link' => [
                  '#title' => $cell_date_value,
                  '#type' => 'link',
                  '#url' => $cell_date_url,
                  '#attributes' => [
                    'aria-label' => 'See events on ' . date('F j, Y', $cell_date),
                    'class' => ['event-calendar-cell-has-events'],
                  ],
                ],
              ];
            } else {
              $td['content'] = [
                '#type' => 'html_tag',
                '#tag' => 'span',
                '#value' => $cell_date_value,
                '#attributes' => [
                  'class' => ['event-calendar-cell-no-events'],
                ]
              ];
            }

            $td['#attributes'] = [
              'data-date' => $cell_date_formatted,
            ];
          } else {
            $td['content'] = [
              '#type' => 'html_tag',
              '#tag' => 'span',
              '#value' => '&nbsp;',
              '#attributes' => [
                'class' => ['event-calendar-cell-empty'],
              ]
            ];
          }

          $tr[] = $td;
        }
        $table['tbody']['tr_' . $row] = $tr;
      }

      // Add table to calendar filter container.
      $build['calendar_filter'][] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => [
            'calendar-filter-month',
          ],
        ],
        'month_table' => $table,
      ];

    }

    return Drupal::service('renderer')->render($build);
  }

  private function convertSmartDate($timestamp, $src_timezone, $dest_timezone): int
  {

    if (empty($src_timezone)) {
      $src_timezone = date_default_timezone_get();
    }

    if (empty($dest_timezone)) {
      $dest_timezone = DateTimeItemInterface::STORAGE_TIMEZONE;
    }

    // Change the timezone of the field value.
    $src_dt = date('Y-m-d H:i:s', $timestamp);
    $source_timezone =  new DateTimeZone($src_timezone);
    $destination_timezone = new DateTimeZone($dest_timezone);
    $dt = new DrupalDateTime($src_dt, $source_timezone);
    $dt->setTimeZone($destination_timezone);

    return strtotime($dt->format('Y-m-d H:i:s'));
  }

}

